package app.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
* Trida reprezentujici menu hry
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class MenuView extends JMenuBar {

	private static MenuView menuBar;
	
	private MenuView() {}
	
	public static MenuView createMenu() {
		if(menuBar == null) {
			menuBar = new MenuView();
			menuBar.addItems();
		}
				
		return menuBar;
	}
	
	private void addItems() {
		JMenu mnHra = new JMenu("Hra");
		add(mnHra);
		JMenuItem mntmNew = new JMenuItem("Nová hra"); // Nova hra
		mnHra.add(mntmNew);		
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewGameView.showNewGamePanel();	// Zobrazim formular pro novou hru
			}
		});
		JMenuItem mntmLoad = new JMenuItem("Nahrát hru"); // Nahrat
		mnHra.add(mntmLoad);	
		JMenuItem mntmSave = new JMenuItem("Uložit hru"); // Ulozit
		mnHra.add(mntmSave);	
		JMenuItem mntmClose = new JMenuItem("Zavřít"); // Zavrit
		mnHra.add(mntmClose);	
		
		// Edidace
		JMenu mnEdit = new JMenu("Editovat");
		add(mnEdit);
		JMenuItem mntmEdit = new JMenuItem("Krok zpět");	// Zpet
		mnEdit.add(mntmEdit);

		// Napoveda
		JMenu mnHelp = new JMenu("Nápověda");
		add(mnHelp);
		JMenuItem mntmRules = new JMenuItem("Pravidla"); // Pravidla
		mnHelp.add(mntmRules);
		JMenuItem mntmCreators = new JMenuItem("Tvůrci"); // Tvurci
		mnHelp.add(mntmCreators);
		
		new FileDialogs(mntmSave, mntmLoad);
	}
}
