package app.view;

import javax.swing.JLabel;

import app.controller.FreeCardController;

/**
* Trida reprezentujici zobrazeni volne karty
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class FreeCardView extends JLabel {
	// Label pro volnou kartu
	private static FreeCardView labelFreeCard;
	private FreeCardController controller;
	
	/**
	 * Vytvori label s volnou kartou
	 */
	private FreeCardView() {
		setText("Volna karta:");
		setController();
		addMouseListener(controller);
		setIcon(FreeCardController.getFreeCardIcon());
	}
	
	/**
	 * Vytvori novy label, pokud neexistuje
	 * 
	 * @return Label reprezentujici volnou kartu
	 */
	public static FreeCardView createFreeCardView() {
		if(labelFreeCard == null) {
			labelFreeCard = new FreeCardView();
		}
		return labelFreeCard;
	}
	
	/**
	 * Nastavi novy kontroler
	 */
	private void setController() {
		controller = new FreeCardController();
	}
	
	/**
	 * Provede zmenu volne karty
	 */
	public static void changeFreeCard() {
		labelFreeCard.setIcon(FreeCardController.getFreeCardIcon());
	}
}
