package app.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import app.model.Game;

/**
* Trida reprezentujici formular pro vyplneni nastaveni nove hry
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class NewGameView extends JPanel{
	
	// Panel s formularem nove hry
	private static NewGameView newGamePanel;
	
	/**
	 * Konstruktor
	 */
	private NewGameView() {}
	
	/**
	 * Vytvorim formular pro novou hru
	 * @brief	Formular se bude sbirat 
	 * 			-pocet hracu
	 * 			-pocet poli
	 * 			-pocet pokladu
	 * 
	 * @return JPanel - hotovy formular
	 */
	private static NewGameView createNewGamePanel() {
		// Vytvorim panel pro formular na novou hru
		if(newGamePanel == null) {
			newGamePanel = new NewGameView();
			newGamePanel.addItems();
		}		
		// Vratim hotovy panel
		return newGamePanel;
	}
	
	/**
	 * Prida polozky do formulare pro novou hru
	 */
	private void addItems() {
		// Formular pro pocet hracu
		JLabel lblPoetHr = new JLabel("Players:");
		add(lblPoetHr);
		JTextField numPlayers;
		numPlayers = new JTextField();
		add(numPlayers);
		numPlayers.setColumns(5);
		
		// Formular pro pocet policek
		JLabel lblNewLabel = new JLabel("Fields");
		add(lblNewLabel);
		JTextField numFields;
		numFields = new JTextField();
		numFields.setText("");
		add(numFields);
		numFields.setColumns(5);
		
		// Formular pro pocet pokladu
		JLabel lblNumTre = new JLabel("Treasure:");
		add(lblNumTre);
		JTextField numTre;
		numTre = new JTextField();
		add(numTre);
		numTre.setColumns(5);
		
		// Tlacitko pro vytvoreni hry
		JButton btnNew = new JButton("Vytvoř");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(
						"Spoustim hru s " + numPlayers.getText() + 
						" velikosti pole " + numFields.getText() + 
						"*" + numFields.getText() + 
						" a poctem pokladu " + numTre.getText()
						);
				// Ziskane hodnoty
				int players = Integer.parseInt(numPlayers.getText());
				int fields = Integer.parseInt(numFields.getText());
				int treasures = Integer.parseInt(numTre.getText());
				
				//Zkusim vytvorit hru
				if(!Game.createGame(players, fields, treasures)) {
					JOptionPane.showMessageDialog(MainView.getInitView(),
						    "Počet hráčů může být 2-4.\nPočet polí 5 - 11 a zároveň pouze lichá čísla.\nPočet poladů 12 nebo 24");
				}
				// Odstanim panel z hlavniho okna
				removeNewGamePanel();
			}
		});
		btnNew.setHorizontalAlignment(SwingConstants.LEADING);
		add(btnNew);
	}
	
	/**
	 * Metoda zobrazi panel pro novou hru
	 */
	public static void showNewGamePanel() {
		createNewGamePanel();
		MainView mainWindow = MainView.getInitView();
		// Zviditelnim panel
		newGamePanel.setVisible(true);
		// Vlozim panel dolu
		mainWindow.getContentPane().add(newGamePanel, BorderLayout.SOUTH);
		mainWindow.revalidate();
	}
	
	/**
	 * Metoda odstrani kontejnery z panelu a panel z hlavniho okna
	 */
	public void removeNewGamePanel() {
		MainView mainWindow = MainView.getInitView();
		mainWindow.getContentPane().remove(newGamePanel);
		newGamePanel.removeAll();
		newGamePanel = null;
	}
}
