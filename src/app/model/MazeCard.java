package app.model;

/**
 * Trida reprezentujici kontroler pro volnou kartu
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class MazeCard{
	
	// Smery karty v poradi UP, RIGHT, DOWN, LEFT
    public static final boolean[] C_PATTERN = {true, false, false, true};
    public static final boolean[] F_PATTERN = {true, true, false, true};
    public static final boolean[] L_PATTERN = {false, true, false, true};
    
    // ID karty
    public static final int C_CODE = 1;
    public static final int L_CODE = 2;
    public static final int F_CODE = 0;
    
    // Uhly na otoceni karet (0-270)
    public static final int ANGLE_0 = 0;
    public static final int ANGLE_90 = 1;
    public static final int ANGLE_180 = 2;
    public static final int ANGLE_270 = 3;
	
	private int patternId;	// Cislo karty - 0 = C , 1 = T, 2 = L
    private int rotate;	// Rotace karty 0 = 0, 1 = 90, 2 = 180, 3 = 270
    private Treasure treasure;	// Poklad karty
    private boolean[] cardPattern = new boolean[4]; // Aktualni smer karty
    
    public static enum CANGO {
        LEFT, UP, RIGHT, DOWN
    }
    
    /**
     * Konstruktor pro vytvoreni karty
     */
    private MazeCard() {
    	treasure = null;
    }
    
    /**
     * Vygeneruje kartu
     * 
     * @param code - Kod karty (0-2)
     * @param angle - Typ uhlu (0-3)
     * @return	karta
     */
    public static MazeCard create(int code, int angle) {
        MazeCard tmp = new MazeCard();  
        
        tmp.createCardPattern(code, angle);
        
        return tmp;
    }
    
    /**
     * Vytvori pozadovany vzor v poli
     * 
     * @param pattern - Vzor karty (C, F nebo L)
     * @param angle - Uhel natoceni
     * @param id - Identifikace vzoru
     */
    private void createCardPattern(int patternId, int angle) {
    	boolean[] pattern;
    	
    	switch (patternId) {
		case C_CODE:
			pattern = C_PATTERN; break;
		case F_CODE:
			pattern = F_PATTERN; break;
		default:
			pattern = L_PATTERN; break;
		}
    	
    	cardPattern[0] = pattern[0];
    	cardPattern[1] = pattern[1];
    	cardPattern[2] = pattern[2];
    	cardPattern[3] = pattern[3];
        
        this.patternId = patternId;
        
        // Jeden cyklus je otoceni o 90 stupnu
        for(int i = 0; i < angle; i++) {
            turnCardRight();
        }
    }
    
    /**
     * Metoda urci zda lze jit danym smerem
     * 
     * @param direction - Karta
     * @return true = lze, false = nelze
     */
    public boolean canGo(MazeCard.CANGO direction) {
        switch(direction) {
            case LEFT:
                return cardPattern[3];
            case DOWN:
                return cardPattern[2];
            case RIGHT:
                return cardPattern[1];
            case UP:
                return cardPattern[0];
            default:
                throw new IllegalArgumentException();
        }
    }
    
    /**
     *	Otoci kartu o 90 stupnu
     */
    public void turnCardRight() {
        boolean tmp = cardPattern[3];
        cardPattern[3] = cardPattern[2];
        cardPattern[2] = cardPattern[1];
        cardPattern[1] = cardPattern[0];
        cardPattern[0] = tmp;
        
        if(rotate == ANGLE_270) { // Po 270 stupnich nasleduje 0
        	rotate = ANGLE_0; 
        }
        else {
        	rotate++;
        }
    }
    
    /**
     * 
     * @return cislo karty
     */
    public int getPatternId() {
    	return patternId;
    }
    
    /**
     * 
     * @return rotaci karty
     */
    public int getRotate() {
    	return rotate;
    }
    
    /**
     * 
     * @return poklad karty
     */
    public Treasure getTreasure() {
    	return treasure;
    }
    
    /**
     * Nastavi karte poklad
     * 
     * @param treasure - Poklad na ktery nastavit kartu
     */
    public void setTreasure(Treasure treasure) {
    	this.treasure = treasure;
    }
}
