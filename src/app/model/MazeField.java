package app.model;

import java.awt.Point;
import java.io.Serializable;

/** Trida reprezentujici policko na desce
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class MazeField implements Serializable{
	
	private static final long serialVersionUID = 3528580522800311531L;
	
	private Point position;
	
	private MazeCard card;	// Karta na policku
	
	/**
	 * Vytvori policko
	 * 
	 * @param row - Na radku
	 * @param col - Na sloupci
	 */
	public MazeField(int row, int col) {
		position = new Point(row, col);
	    card = null;	// Zatim zadna karta
	}
	
	/**
	 * 
	 * @return radek policka
	 */
	public int getRow() {
	    return (int) position.getX();
	}
	
	/**
	 * 
	 * @return sloupec policka
	 */
	public int getCol() {
	    return (int) position.getY();
	}
	
	/**
	 * @param position - Pozice policka
	 */
	public void setPosition(Point position) {
		this.position = position;
	}
	
	/**
	 * 
	 * @return position of field
	 */
	public Point getPosition() {
		return position;
	}
	
	/**
	 * 
	 * @return	aktualni kartu na policku
	 */
	public MazeCard getCard() {
	    return card;
	}
	
	/**
	 * 
	 * @param actualCard - aktualni karta na policku
	 */
	public void putCard(MazeCard actualCard) {
	    card = actualCard;
	}
	
	/**
	 * Posune policko smerem nahoru nebo dolu
	 * 
	 * @param direction - Smer kterym posunout (zapor = nahoru, klad = dolu)
	 */
	public void moveVertical(int direction) {
		position.x += direction;
		checkSpecialState();
	}
	
	/**
	 * Posune policko smerem vlevo nebo doprava
	 * 
	 * @param direction - Smer kterym posunout (zapor = doleva, klad = doprava)
	 */
	public void moveHorizontal(int direction) {
		position.y += direction;
		checkSpecialState();
	}
	
	private void checkSpecialState() {
		if(position.x == 0 || position.y == 0) {
			position.setLocation(0, 0);
		}
	}
}
