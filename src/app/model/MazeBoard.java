package app.model;

import java.io.Serializable;

import app.view.CardView;

/** 
 * Třída reprezentuje hraci desku.
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class MazeBoard implements Serializable{

	private static final long serialVersionUID = 8154537722065501684L;

	// Konstanty pro pohyby po desce
	private static final int LEFT = -1;
	private static final int RIGHT = 1;
	private static final int UP = -1;
	private static final int DOWN = 1;
	
	// Deska
	public static MazeBoard mazeBoard;
	
	// Naposled shiftovane policko
	private static MazeField lastShiftedField;	
	
	// Volna karta
	private static MazeCard freeCard;
	
	// Popis desky
	private static MazeField[][] fieldsArray;	// Policka
	
	private int sizeOfBoard;	// Velikost desky r*c
	
	/**
	 * Vytvori novou desku
	 * 
	 * @param numOfFields - Velikost desky numOfFields x numOfFields
	 * @param game
	 */
	private MazeBoard(int numOfFields) {
		fieldsArray = new MazeField[numOfFields + 1][numOfFields + 1];	// Matice policek
		sizeOfBoard = numOfFields;	// Velikost hraci desky
   }
	
   /**
    * Vytvori hraci desku
	*
    * @param numOfFields - Pocet poli
    * 
    * @return MazeBoard - Hraci desku
    */
	public static MazeBoard createMazeBoard(int numOfFields) {
		if(mazeBoard == null) {
			mazeBoard = new MazeBoard(numOfFields);
			mazeBoard.createBoardFields();
		}
		return mazeBoard;
	}

	/**
	 * Nastavi pole policek
	 */
	private void createBoardFields() {
		// Vytvorim policka
		for(int row = 1; row <= sizeOfBoard; row++) {
           for(int col = 1; col <= sizeOfBoard; col++) {
        	   fieldsArray[row][col] = new MazeField(row, col);
           }
		}
	}
	
	/**
	 * Nahraje poklad hrace + poklad na policku
	 */
	public static void showTreasureUnderPlayer() {
		Player playerOnTurn = Turn.getPlayerOnTurn();
		
		// Nahraji poklad na pozici hrace
		MazeField playerField = playerOnTurn.getActualField();
		MazeCard fieldCard = playerField.getCard();
		Treasure foundedTreasure = fieldCard.getTreasure();
		
		if(foundedTreasure != null) {
			if(playerOnTurn.compareTreasures(foundedTreasure)) {
				fieldCard.setTreasure(null);
				System.out.printf("Nasel jsem\n");
				
				int numTreasure = Treasure.getNumOfTreasure();
				int numPlayers = Player.getPlayers().size();
				// Zkontroluji zda jiz hrac nevyhral
				if(playerOnTurn.getPoints() >= numTreasure / numPlayers){
					Game.setHasWinner();
					System.out.printf("Vyhra\n");
				}
				CardView.setPlayerCardIcon();
			}
		}
	}
   
	/**
	 * Provedu shift
	 * 
	 * @param clickedField - policko na ktere chci vlozit volny kamen
	 * 
	 */
	public void shift(MazeField clickedField){
		System.out.println("shiftuji");
		
		if(!canShift(clickedField)) {
			return;
		}
		
		int clickedRow = clickedField.getRow();
		int clickedCol = clickedField.getCol();
		boolean pushed = false;
		
		MazeCard lastShiftedCard = null;
		MazeField newField;
		MazeField oldField;
		
		if(clickedCol % 2 == 0) { // Lichy sloupec
			if(clickedRow == 1) { // Horni radek
				lastShiftedField = fieldsArray[sizeOfBoard][clickedCol];
				lastShiftedCard = lastShiftedField.getCard(); // Karta kterou pushuji
				
				savePushedPlayer(lastShiftedField);
				
				for(int i = sizeOfBoard; i > 1; i--) {
					newField = fieldsArray[i][clickedCol];
					oldField = fieldsArray[i+UP][clickedCol];
					pushFields(newField, oldField);
				}
	            pushed = true;
			}
			else if(clickedRow == sizeOfBoard) { // Spodni radek
				lastShiftedField = fieldsArray[1][clickedCol];
				lastShiftedCard = lastShiftedField.getCard(); // Karta kterou pushuji
				
				savePushedPlayer(lastShiftedField);
				
				for(int i = 1; i < sizeOfBoard; i++){
					newField = fieldsArray[i][clickedCol];
					oldField = fieldsArray[i+DOWN][clickedCol];
					pushFields(newField, oldField);
				}
				pushed = true;
			}
			
		}
		else if(clickedRow % 2 == 0) { // Lichy radek
			if(clickedCol == 1) { // Levy sloupec
				lastShiftedField = fieldsArray[clickedRow][sizeOfBoard];
				lastShiftedCard = lastShiftedField.getCard(); // Karta kterou pushuji
				
				savePushedPlayer(lastShiftedField);
				
				for(int i = sizeOfBoard; i > 1; i--){
					newField = fieldsArray[clickedRow][i];
					oldField = fieldsArray[clickedRow][i+LEFT];
					pushFields(newField, oldField);
				}
				pushed = true;
			}
			else if(clickedCol == sizeOfBoard) { // Pravy sloupec
				lastShiftedField = fieldsArray[clickedRow][1];
				lastShiftedCard = lastShiftedField.getCard(); // Karta kterou pushuji
				
				savePushedPlayer(lastShiftedField);
	
				for(int i = 1; i < sizeOfBoard; i++){
					newField = fieldsArray[clickedRow][i];
					oldField = fieldsArray[clickedRow][i+RIGHT];
					pushFields(newField, oldField);
				}
				pushed = true;
			}
		}
		
		if(pushed) {
			loadPushedPlayer(clickedField);
			swapPushedFree(clickedField, lastShiftedCard);
		}
	}
	
	/**
	 * 
	 * @param clickedField - Uzivatel zvolene policko
	 * @return false - pri pokusu o pushnuti naposled pushnuteho, jinak true
	 */
	private boolean canShift(MazeField clickedField) {
		if(lastShiftedField != null && lastShiftedField.equals(clickedField)) {
	        	return false;
		}
		return true;
	}
	
	/**
	 * Schova hrace, ktery se nachazi na pushovanem policku
	 * 
	 * @param lastShift - policko, ktere bude vytahnuto
	 */
	public void savePushedPlayer(MazeField lastShift) {
		int numOfPlayers = Player.getNumOfPlayers();
		Player tmpPlayer;
		MazeField playerPos;
		
		for(int p = 0; p < numOfPlayers; p++) { // Schovam pokud byl hrac na posledni karte
			tmpPlayer = Player.getPlayer(p);
			playerPos = tmpPlayer.getActualField();
			if(playerPos.equals(lastShift)) {
				tmpPlayer.setPushed(true);
			}
		}
	}
	
	/**
	 * Pushne karty policek
	 * 
	 * @param newField - Policko na novem miste
	 * @param oldField - Policko na starem miste
	 */
	public void pushFields(MazeField newField, MazeField oldField) {
		pushPlayer(oldField, newField);
		
		Turn.setShift(false);
		newField.putCard(oldField.getCard());
	}
	
	/**
	 * Nacte ulozeneho hrace
	 * 
	 * @param clickedField - Uzivatelem naklikle policko
	 */
	public void loadPushedPlayer(MazeField clickedField) {
		int numOfPlayers = Player.getNumOfPlayers();
		Player pushedPlayer;
		
		for(int i = 0; i < numOfPlayers; i++){
			pushedPlayer = Player.getPlayer(i);
			pushedPlayer.isPushed(clickedField);
		}
	}
	
	/**
	 * Nahraje volnout kartu na desku a vytahnutou nahraje jako volnou
	 * 
	 * @param clickedField - Uzivatelem vkladane policko
	 * @param pushedCard - Vytahle policko z lajny
	 */
	public void swapPushedFree(MazeField clickedField, MazeCard pushedCard) {
		clickedField.putCard(freeCard);
		freeCard = pushedCard;
	}
	
	/**
	 * Pokud se hrac nachazi na posunovanem policku, je premisten
	 * 
	 * @param oldField - Presouvane policko
	 * @param newField - Nove policko
	 */
	public void pushPlayer(MazeField oldField, MazeField newField) {
		int numOfPlayers = Player.getNumOfPlayers();
		Player tmpPlayer;
		MazeField playerPos;
		
		for(int p = 0; p < numOfPlayers;p++){
			tmpPlayer = Player.getPlayer(p);
			playerPos = tmpPlayer.getActualField();
			if(playerPos.equals(oldField)) {
				tmpPlayer.setActualField(newField);
			}
		}
	}
	
	/**
	 * Vlozi hrace na startovani pozice
	 * 
	 * @param playerId - Jedinecne cislo hrace
	 * @return MazeField - startovni policko hrace
	 */
	public MazeField getPlayerStartField(int playerId) {
		// Mozne pozice
		int TOP = 1;
		int LEFT = 1;
		int RIGHT = sizeOfBoard;
		int BOT = sizeOfBoard;
		
		MazeField startField;
		
		// Usadim hrace
		switch (playerId) {
	        case 0:
	        	startField = getField(TOP, LEFT); break;	// LEVO HORE
	        case 1:
	        	startField = getField(TOP, RIGHT); break; // PREVO HORE
	        case 2:
	        	startField = getField(BOT, LEFT); break; // LEVO DOLE
	        case 3:
	        	startField = getField(BOT, RIGHT); break; // PRAVO DOLE
	        default:
	            throw new IllegalArgumentException();
		}
		return startField;
	}
	
	public MazeField[][] getFieldsArray() {
		return fieldsArray;
	}
	
	/**
	 * @return MazeBoard - Vraci aktualni hraci desku
	 */
	public static MazeBoard getMazeBoard() {
		return mazeBoard;
	}
	
	/**
	 * @return MazeCard - Vrati aktualni volnou kartu
	 */
	public MazeCard getFreeCard() {
		return freeCard;
	}

	/**
	 * @param posX X - souradnice
	 * @param posY Y - souradnice
	 * 
	 * @return MazeField - Vrati pozadovane pole
	 */
	public static MazeField getField(int posX, int posY){
		return fieldsArray[posX][posY];
	}
   
	/**
	 * @return int - Vrati velikost hraci desky
	 */
	public int getSizeOfBoard(){
		return sizeOfBoard;
	}
	
	/**
	 * Nastavi volnou kartu
	 * 
	 * @param newFreeCard - Karta kterou vlozit
	 */
	public static void setFreeCard(MazeCard newFreeCard) {
		freeCard = newFreeCard;
	}
}
