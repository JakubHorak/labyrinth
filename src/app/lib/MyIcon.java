package app.lib;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import app.model.MazeBoard;
import app.model.MazeCard;
import app.model.MazeField;
import app.model.Player;
import app.view.BoardView;

/**
 * Trida reprezentujici knihovnu pro praci s ikonami
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public final class MyIcon {
	
	// Pruhledny obrazek pro prekreslovani
	public final static String LIB_PATH = "lib/";
	private final static ImageIcon ALPHA_IMAGE = new ImageIcon(LIB_PATH + "pruhledny.png");
	// Pole pro poklady, hrace a policka
	private static ImageIcon[] treasureIcons;
	private static ImageIcon[] playerIcons;
	private static ImageIcon[][] fieldIcons;
	
	/**
	 * Soukromy konstruktor
	 */
	private MyIcon(){}
	
	/**
	 * Projde vsechny hrace a na jejich aktualni pozici je nakresli
	 */
	public static void setPlayerIcon() {
		List<Player> players= Player.getPlayers();
		
		Player tmpPlayer;
		JLabel field;
		Image lblImg;
		Image playerImg;
		ImageIcon lblImageIcon;
		Icon newIcon;
		
		for(int i = 0; i < players.size(); i++) {
			// Vezmu hrace
			tmpPlayer = players.get(i);
			
			// Nahraji label na kterem je hrac
			field = BoardView.getLblField(tmpPlayer.getRow(), tmpPlayer.getCol());
			
			// Vytahnu ikonu policka a hrace
			lblImageIcon = (ImageIcon) field.getIcon();
			lblImg = lblImageIcon.getImage();
			playerImg = getPlayerIcon(i).getImage();
			
			// Prekreslim policko obrazkem hrace
			newIcon = redrawImages(lblImg, playerImg);
			field.setIcon(newIcon);
		}
	}
	
	/**
	 * Provede nakresleni hrace na ikonu policka
	 * 
	 * @param labelImg	Obrazek policka(labelu)
	 * @param playerImg Obrazek hrace
	 * 
	 * @return Prekreslena ikona policka
	 */
	private static ImageIcon redrawImages(Image labelImg, Image playerImg) {
		BufferedImage bufferedImage = new BufferedImage(labelImg.getWidth(null), labelImg.getHeight(null),BufferedImage.TYPE_INT_RGB);
		
		Graphics  draw = bufferedImage.createGraphics();
		
	    draw.drawImage(labelImg, 0, 0, null);
	    draw.drawImage(ALPHA_IMAGE.getImage(), 0, 0, null);
	    draw.drawImage(playerImg, 0, 0, null);
	    
	    draw.dispose();
	    
	    ImageIcon myIcon = new ImageIcon(bufferedImage);
	    
	    return myIcon;
	}
	
	/**
	 * Nastavi ikonu pro policka
	 * 
	 * @param row - radek policka
	 * @param col - sloupec policka
	 * 
	 * @return Icon Ikona ktera nalezi policku
	 */
	public static Icon setFieldIcon(int row, int col) {
		// Info policka
		MazeField tmpField = MazeBoard.getField(row, col); // field na dane pozici
		MazeCard fieldCard = tmpField.getCard(); // karta na fieldu
		
		Icon fieldIcon;
		
		fieldIcon = MyIcon.getImageIcon(fieldCard);
		
		return fieldIcon;
	}
	
	/**
	 * Vraci ikonu hrace, inicializuje pole pokud neni
	 * 
	 * @param playerId - Id hrace
	 * 
	 * @return Ikonu odpovidajici id hrace
	 */
	private static ImageIcon getPlayerIcon(int playerId) {
		if(playerIcons == null) {
			initPlayerIcon();
		}
		return playerIcons[playerId];
	}
	
	/**
	 * Vraci ikonu pokladu, inicializuje pole pokud neni
	 * 
	 * @param numOfTreasure - Cislo pokladu
	 * 
	 * @return Ikonu odpovidajici cislu pokladu
	 */
	public static ImageIcon getTreasureIcon(int numOfTreasure) {
		if(treasureIcons == null) {
			initTreasureIcon();
		}
		return treasureIcons[numOfTreasure];
	}
	
	/**
	 * Vraci ikonu policka na zaklade ID karty a jeji rotace
	 * 
	 * @param tmpCard - ID karty
	 * 
	 * @return ImageIcon - Ikonu policka daneho typu a rotace
	 */
	public static ImageIcon getImageIcon(MazeCard tmpCard) {
		if(fieldIcons == null) {
			initFieldIcon();
		}
		
		int cardId = tmpCard.getPatternId(); // ID
		int cardRot = tmpCard.getRotate(); // Rotace
		
	    return fieldIcons[cardId][cardRot];
	}
	
	/**
	 * Inicializuje pole ikon hrace
	 */
	private static void initPlayerIcon() {
		
		playerIcons = new ImageIcon[4];
		
		playerIcons[0] = new ImageIcon(LIB_PATH + "player1.png");
		playerIcons[1] = new ImageIcon(LIB_PATH + "player2.png");
		playerIcons[2] = new ImageIcon(LIB_PATH + "player3.png");
		playerIcons[3] = new ImageIcon(LIB_PATH + "player4.png");
	}
	
	/**
	 * Inicializuje pole ikon policek
	 */
	private static void initFieldIcon() {
		
		fieldIcons = new ImageIcon[4][4];
		
		// T=F[rotace]
		fieldIcons[0][0] = new ImageIcon(LIB_PATH + "T0.png");//.getImage();
		fieldIcons[0][1] = new ImageIcon(LIB_PATH + "T1.png");
		fieldIcons[0][2] = new ImageIcon(LIB_PATH + "T2.png");
		fieldIcons[0][3] = new ImageIcon(LIB_PATH + "T3.png");
		// L=C[rotace]
		fieldIcons[1][0] = new ImageIcon(LIB_PATH + "L0.png");
		fieldIcons[1][1] = new ImageIcon(LIB_PATH + "L1.png");
		fieldIcons[1][2] = new ImageIcon(LIB_PATH + "L2.png");
		fieldIcons[1][3] = new ImageIcon(LIB_PATH + "L3.png");
		// I=L [rotace]
		fieldIcons[2][0] = new ImageIcon(LIB_PATH + "I0.png");
		fieldIcons[2][1] = new ImageIcon(LIB_PATH + "I1.png");
		fieldIcons[2][2] = new ImageIcon(LIB_PATH + "I0.png");
		fieldIcons[2][3] = new ImageIcon(LIB_PATH + "I1.png");
	}
	
	/**
	 * Inicializuje pole ikon pokladu
	 */
	private static void initTreasureIcon() {
		
		treasureIcons = new ImageIcon[25];
		
		treasureIcons[0] = new ImageIcon(LIB_PATH + "tr0.png");
		treasureIcons[1] = new ImageIcon(LIB_PATH + "tr1.png");
		treasureIcons[2] = new ImageIcon(LIB_PATH + "tr2.png");
		treasureIcons[3] = new ImageIcon(LIB_PATH + "tr3.png");
		treasureIcons[4] = new ImageIcon(LIB_PATH + "tr4.png");
		treasureIcons[5] = new ImageIcon(LIB_PATH + "tr5.png");
		treasureIcons[6] = new ImageIcon(LIB_PATH + "tr6.png");
		treasureIcons[7] = new ImageIcon(LIB_PATH + "tr7.png");
		treasureIcons[8] = new ImageIcon(LIB_PATH + "tr8.png");
		treasureIcons[9] = new ImageIcon(LIB_PATH + "tr9.png");
		treasureIcons[10] = new ImageIcon(LIB_PATH + "tr10.png");
		treasureIcons[11] = new ImageIcon(LIB_PATH + "tr11.png");
		treasureIcons[12] = new ImageIcon(LIB_PATH + "tr12.png");
		treasureIcons[13] = new ImageIcon(LIB_PATH + "tr13.png");
		treasureIcons[14] = new ImageIcon(LIB_PATH + "tr14.png");
		treasureIcons[15] = new ImageIcon(LIB_PATH + "tr15.png");
		treasureIcons[16] = new ImageIcon(LIB_PATH + "tr16.png");
		treasureIcons[17] = new ImageIcon(LIB_PATH + "tr17.png");
		treasureIcons[18] = new ImageIcon(LIB_PATH + "tr18.png");
		treasureIcons[19] = new ImageIcon(LIB_PATH + "tr19.png");
		treasureIcons[20] = new ImageIcon(LIB_PATH + "tr20.png");
		treasureIcons[21] = new ImageIcon(LIB_PATH + "tr21.png");
		treasureIcons[22] = new ImageIcon(LIB_PATH + "tr22.png");
		treasureIcons[23] = new ImageIcon(LIB_PATH + "tr23.png");
		treasureIcons[24] = new ImageIcon(LIB_PATH + "otaznik.png");
	}
}
