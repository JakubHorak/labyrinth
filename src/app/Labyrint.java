package app;

import app.view.MainView;

/**
* Main trida aplikace
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
public class Labyrint {

	/**
	 * Spusti program
	 * 
	 * @param args - Spusteci parametry
	 */
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new MainView();
            }
        });
	}
}
