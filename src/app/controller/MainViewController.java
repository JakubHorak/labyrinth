package app.controller;

import javax.swing.JOptionPane;

import app.model.Game;
import app.model.MazeBoard;
import app.model.Turn;
import app.view.CardView;
import app.view.MainView;
import app.view.StatusView;

/** 
 * Trida reprezentujici kontroler hlavniho okna
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class MainViewController {
	
	// Odkaz na sebe
	private static MainViewController controller;
	// Data hry
	public MazeBoard mazeBoard; 
	// Obraz hry
	private MainView myWindow;
	
	/**
	 * Vytvori komunikacni kanaly
	 */
	public MainViewController(){
		controller = this;
	}
	
	/** 
	 * Dalsi hrac
	 */
	public void nextTurn() {
		// Dalsi tah
		Game.nextTurn();
		
		// Aktualizuj CardView
		CardView.setFieldCardIcon();
		CardView.setPlayerCardIcon();
		
		// Aktualizuj StatusView
		StatusView.refreshStatusBar();	
	}
	
	/**
	 * Zkontroluje zda uz existuje vitez
	 */
	public void checkWin() {
		if(Game.isWinner()) {
        	JOptionPane.showMessageDialog(myWindow,
				    "Vítězem se stává hráč " + Turn.getPlayerOnTurn());
        }
	}
	
	/**
	 * 
	 * @return Vrati hlavni kontroler
	 */
	public static MainViewController getController() {
		return controller;
	}
}
