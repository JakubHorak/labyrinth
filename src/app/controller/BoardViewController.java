package app.controller;

import java.awt.event.MouseEvent;

import javax.swing.JComponent;

import app.model.MazeBoard;
import app.model.MazeCard;
import app.model.MazeField;
import app.model.Player;
import app.model.Turn;
import app.view.BoardView;
import app.view.CardView;
import app.view.FreeCardView;
import app.view.StatusView;

/**
 * Trida reprezentujici kontroler hraci desky
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class BoardViewController implements java.awt.event.MouseListener{
	
	// View hraci desky
	private BoardView boardView;
	private MazeBoard mazeBoard;

	/**
	 * Vytvori kontroler pro tridu @class BoardView
	 * 
	 * @param mazeBoard - Hraci deska
	 * @param boardView - Zobrazeni hraci desky
	 */
	public BoardViewController(MazeBoard mazeBoard, BoardView boardView) {
		this.mazeBoard = mazeBoard;
		this.boardView = boardView;
	}
	/** Shift nebo posun hrace
	 * 
	 * @param clickedField - pole na ktere hrac klikl
	 */
	public void boardAction(MazeField clickedField){
        if(Turn.isShift()) {	// Pokud hrac neshiftoval
        	mazeBoard.shift(clickedField);
        }
        else { // Zkousi se presunout
        	tryGo(clickedField);    
    	}	
	}
	
	/**	Metoda zavola metodu move() se smerem, kterym chce jit hrac
     * 	
     * @param frow - X souradnice kam chce vstoupit hrac
     * @param fcol - Y souradnice kam chce vstoupit hrac
     * @param tmpPlayer - objekt prave hrajiciho hrace
     * @return True muze jit, False nemuze
     */
    private void tryGo(MazeField clickedField){
    	Player tmpPlayer = Turn.getPlayerOnTurn(); // Ziskat hrace co je prave na tahu
    	
    	// Nahraji kliknute pole
    	int posX = tmpPlayer.getRow();
    	int posY = tmpPlayer.getCol();
    	
    	int clickedRow = clickedField.getRow();
    	int clickedCol = clickedField.getCol();
    	
    	
    	if(clickedRow == posX) { // Stejny radek
    		if(clickedCol - posY == -1) {
    			tmpPlayer.move(clickedField, MazeCard.CANGO.LEFT); // Zaslu zda mohu jit doleva a cekam
    		}
    		else if(clickedCol - posY == 1) {
    			tmpPlayer.move(clickedField, MazeCard.CANGO.RIGHT); // Zaslu zda mohu jit doprava a cekam
    		}
    	}
    	else if(clickedCol - posY == 0) {	// Stejny sloupec
    		if(clickedRow - posX == -1) {
    			tmpPlayer.move(clickedField, MazeCard.CANGO.UP); // Zaslu zda mohu jit nahoru a cekam
    		}
    		else if(clickedRow - posX == 1) {
    			tmpPlayer.move(clickedField, MazeCard.CANGO.DOWN); // Zaslu zda mohu jit dolu a cekam
    		}
    	}
    }
    
	@Override
	public void mousePressed(MouseEvent e) {
		JComponent c = (JComponent) e.getSource();
		// Prevedu souradnice na jednotlive policka
        int row = c.getY() / 50 + 1;
        int col = c.getX() / 50 + 1;
        MazeField clickedField = MazeBoard.getField(row, col);

        // Chci shift
        boardAction(clickedField);
        
        // Refresh
        StatusView.refreshLblPlayerPoints();
        FreeCardView.changeFreeCard();
        
        boardView.showBoard(); // Prekreslim desku
      
        CardView.setFieldCardIcon();
		CardView.setPlayerCardIcon();
		
		 // Zkontroluji zda hrac nevyhral
		MainViewController controller = MainViewController.getController();
        controller.checkWin();
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

}
