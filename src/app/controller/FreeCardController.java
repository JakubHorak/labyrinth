package app.controller;

import java.awt.event.MouseEvent;

import javax.swing.Icon;

import app.lib.MyIcon;
import app.model.MazeBoard;
import app.model.MazeCard;
import app.view.FreeCardView;

/**
 * Trida reprezentujici kontroler pro volnou kartu
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class FreeCardController implements java.awt.event.MouseListener{
	
	/**
	 * @return Icon ktera se ma zobrazit na volne karte
	 */
	public static Icon getFreeCardIcon() {
		MazeBoard board = MazeBoard.getMazeBoard();
		MazeCard freeCard = board.getFreeCard() ; // Nahraji si kartu na nem
		
		return MyIcon.getImageIcon(freeCard);
	}
	
	/**
	 * Udalost rotuje s volnou kartou
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		MazeBoard board = MazeBoard.getMazeBoard();
		
		// Nahraji kartu volneho policka
		MazeCard freeCard = board.getFreeCard();
		
		// Rotuji o 90 stupnu doprava
		freeCard.turnCardRight();
		
		// Zobrazim vysledek
		FreeCardView.changeFreeCard();
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}
}
