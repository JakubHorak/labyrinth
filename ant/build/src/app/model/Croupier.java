package app.model;

import java.util.Random;

/**
* Trida reprezentujici krupiera, ktery poklada karty na hraci desku
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
public final class Croupier {
	
	// Konstanty pro pocty typu patternu a uhlu
	private static final int NUM_PATTERNS = 3;
	private static final int NUM_ANGLES = 4;
	
	// Konstanty pro MIN/MAX velikosti hraci desky
	private static final int TOP_SIDE = 1;
	private static final int LEFT_SIDE = 1;
	private static int BOT_SIDE;
	private static int RIGHT_SIDE;
	
	// Informace o hraci desce
	private static int sizeOfBoard;
	private static MazeField[][] fieldsArray;
	
	/**
	 * Polozi kartu na hraci desku
	 * 
	 * @param sizeOfBoard - Velikost hraci desky
	 * @param fieldsArray - Pole policek hraci desky
	 */
	public static void putCards(int sizeOfBoard, MazeField[][] fieldsArray) {

		Croupier.sizeOfBoard = sizeOfBoard;
		Croupier.fieldsArray = fieldsArray;
		BOT_SIDE = sizeOfBoard;
		RIGHT_SIDE = sizeOfBoard;
		
		onEvenNumFields();
		
		onBorderOddNumFields();
		
		onOddNumFields();
		
		onCornerFields();
		
		onFreeCard();
	}
	
	/**
	 * Polozi karty na sude radky a sloupce
	 */
	private static void onEvenNumFields() {
		Random random = new Random();		
		int randomNumber;
		
		MazeCard newCard;
		// Vsechny sude sloupce jsou nahodne C
		for(int row = 1; row <= sizeOfBoard; row++) {
			for(int col = 2; col <= sizeOfBoard; col += 2) {
				randomNumber = random.nextInt(NUM_PATTERNS);
				newCard = MazeCard.create(randomNumber, MazeCard.ANGLE_0);
			    fieldsArray[row][col].putCard(newCard);
			}
		}
		
		// Vsechny sude sloupce a sude radky jsou nahodne C
		for(int row = 2; row <= sizeOfBoard; row += 2) {
			for(int col = 1; col <= sizeOfBoard; col += 2){
				randomNumber = random.nextInt(NUM_PATTERNS);
				newCard = MazeCard.create(randomNumber, MazeCard.ANGLE_0);
				fieldsArray[row][col].putCard(newCard);
			}
		}
	}
	
	/**
	 * Nastavi liche krajni policka na Tcka
	 */
	private static void onBorderOddNumFields() {
		MazeCard topSide = MazeCard.create(MazeCard.F_CODE, MazeCard.ANGLE_180);
		MazeCard botSide = MazeCard.create(MazeCard.F_CODE, MazeCard.ANGLE_0);
		MazeCard leftSide = MazeCard.create(MazeCard.F_CODE, MazeCard.ANGLE_90);
		MazeCard rightSide = MazeCard.create(MazeCard.F_CODE, MazeCard.ANGLE_270);
		// Nastavim liche sloupce a radky na krajich na typ T smerem dostredu
		for (int oddNum = 1; oddNum <= sizeOfBoard; oddNum += 2) {
			fieldsArray[TOP_SIDE][oddNum].putCard(topSide);
			fieldsArray[BOT_SIDE][oddNum].putCard(botSide);
			fieldsArray[oddNum][LEFT_SIDE].putCard(leftSide);
			fieldsArray[oddNum][RIGHT_SIDE].putCard(rightSide);
		}
	}
	
	/**
	 * Polozim karty na policka na lichyh radcich a polickach
	 */
	private static void onOddNumFields() {
		Random random = new Random();		
		int randomAngle;
		
		MazeCard newCard;
		// Liche uprostred na nahodny typ T
		for(int row = 3; row < sizeOfBoard; row += 2) {
			for(int col = 3; col < sizeOfBoard; col += 2) {
				randomAngle = random.nextInt(NUM_ANGLES);;
				newCard = MazeCard.create(MazeCard.F_CODE, randomAngle);
				fieldsArray[row][col].putCard(newCard);
			}
		}
	}
	
	/**
	 * Polozim karty na rohove policka
	 */
	private static void onCornerFields() {
		// Vytvorim rohove karty
		MazeCard topLeft = MazeCard.create(MazeCard.C_CODE, MazeCard.ANGLE_180);
		MazeCard topRight = MazeCard.create(MazeCard.C_CODE, MazeCard.ANGLE_270);
		MazeCard botLeft = MazeCard.create(MazeCard.C_CODE, MazeCard.ANGLE_90);
		MazeCard botRight = MazeCard.create(MazeCard.C_CODE, MazeCard.ANGLE_0);
		
		//Polozim je na rohove policka
		fieldsArray[TOP_SIDE][LEFT_SIDE].putCard(topLeft); // Karta C 180
		fieldsArray[TOP_SIDE][RIGHT_SIDE].putCard(topRight); // Karta C 270
		fieldsArray[BOT_SIDE][LEFT_SIDE].putCard(botLeft); // Karta C 90
		fieldsArray[BOT_SIDE][RIGHT_SIDE].putCard(botRight); // Karta C 0
	}
	
	/**
	 * Polozim nahodne vygenerovanou kartu na volnou kartu
	 */
	private static void onFreeCard() {
		Random random = new Random();
		// Vytvorim volnou kartu
		int codeRandom = random.nextInt(NUM_PATTERNS);;
		int angleRandom = random.nextInt(NUM_ANGLES);;
		
		MazeCard newFreeCard = MazeCard.create(codeRandom, angleRandom);
		MazeBoard.setFreeCard(newFreeCard);
	}
}
