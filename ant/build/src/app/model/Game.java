package app.model;

import app.view.MainView;

/** 
 * Trida reprezentujici hru
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 */
public class Game {
	
	private static Game game; // Aktualni hra
	private static boolean isWinner = false; // Vitez hry

	private MazeBoard mazeBoard; // Hraci deska
	private int numOfPlayers; // Pocet hracu
	private int numOfTreasures; // Pocet pokladu
	
	/**
	 * Konstruktor hry
	 */
	private Game() {}
	
	/**
	 * Konstruktor hry
	 * 
	 * @param numOfPlayers - Pocet hracu
	 * @param numOfTreasures - Pocet pokladu
	 */
	private Game(int numOfPlayers, int numOfTreasures) {
		this.numOfPlayers = numOfPlayers;
		this.numOfTreasures = numOfTreasures;
		new Turn();
	}
	
	/**
	 * Vytvori novou hru
	 * 
	 * @param players - Pocet hracu (2-4)
	 * @param fields - Pocet poli (5 - 11 a Liche)
	 * @param treasures - Pocet pokladu (12-24)
	 * 
	 * @return true - pokud jsou splneny podminky, false - pokud nejsou
	 */
	public static boolean createGame(int players, int fields, int treasures) {
		// Kontrola vstupu
		if(players < 2 || players > 4) {
			return false;
		}
		if(fields < 5 || fields > 11 || fields % 2 == 0) {
			return false;
		}
		if(treasures != 12 && treasures != 24) {
			return false;
		}
		
		// Vytvarim
		game = new Game(players, treasures);
		game.createNewGame(fields);
		return true;
	}
	    
	/**
	 * Nastavi parametry hry
	 * 
	 * @param numOfFields - Pocet policek hry
	 */
	private void createNewGame(int sizeOfBoard) {
		// Priradim hre desku
		mazeBoard = MazeBoard.createMazeBoard(sizeOfBoard);
		MazeField[][] fieldsArray = mazeBoard.getFieldsArray();
		
		// Polozim karty na policka
		Croupier.putCards(sizeOfBoard, fieldsArray);
		
		// Vytvorim balicek
		Treasure.createSet(numOfTreasures);
		
		// Polozim poklady na policka
		Treasure.putTreasuresOnFields(sizeOfBoard, fieldsArray);
		
		// Vytvorim hrace
		Player.createPlayers(numOfPlayers, mazeBoard);
		
		// Nastavim pocatek
		Game.nextTurn();
		
		// Zobrazim hraci desku
		MainView.getInitView().showMainView();
	}
	
	/**
	 * Provede dalsi tah
	 */
	public static void nextTurn() {
		// Nastavim aktualniho hrace
		Player nextPlayer = Player.getNextPlayer();
		Turn.setPlayerOnTurn(nextPlayer);
		
		// Nahraji poklady
		MazeBoard.showTreasureUnderPlayer();
		
		// Muze byt shift
		Turn.setShift(true);
		
		// Nastavim poklad na policku
		Turn.setFieldTreasure();
	}
	
	/**
	 * Nastavi vyhru
	 */
	public static void setHasWinner() {
		isWinner = true;
	}
	
	/**
	 * @return true - pokud je vitez, false - pokud ne
	 */
	public static boolean isWinner() {
		return isWinner;
	}
}
