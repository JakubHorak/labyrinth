package app.model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/** Trida reprezentujici poklad
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class Treasure implements Serializable{
	
	private static final long serialVersionUID = -850662856818859099L;
	
	// Seznam pokladu
    private static List<Treasure> treasureList;
    private static Iterator<Treasure> treasureIterator; 
    
    private int code;	// Reprezentace pokladu
    
    /**
     * Vytvori poklad
     * @param code - cislo pokladu
     */
    private Treasure(int code) {
        this.code = code;
    }

    /**
     * Vytvori balik pokladu
     * 
     * @param size - Velikost baliku(12 || 24)
     */
    public static void createSet(int size) {
        treasureList = new ArrayList<>();
        
        for(int i = 0; i < size; i++) {
            treasureList.add(new Treasure(i));
        }
        
        treasureIterator = treasureList.iterator();
    }
    
    /**
	 * Polozi poklady na policka
	 * 
	 * @param sizeOfBoard - Velikost desky
	 * @param fieldList - Pole policek pro pristup ke kartam
	 */
	public static void putTreasuresOnFields(int sizeOfBoard, MazeField[][] fieldList) {
		ArrayList<Point> randomPointsList = new ArrayList<>();
		
		Point newPoint;
		
		for(int row = 1; row <= sizeOfBoard; row++) {
			for(int col = 1; col <= sizeOfBoard; col++) {
				newPoint = new Point(row, col);
				randomPointsList.add(newPoint);
			}
		}
		
		Collections.shuffle(randomPointsList);
		
		int packageSize = treasureList.size();
		int row, col;
		// Budu pokladat pocet pokladu treasure
		for(int i = 0; i < packageSize; i++) {
			newPoint = randomPointsList.get(i);
			row = (int) newPoint.getX();
			col = (int) newPoint.getY();
			
	   		System.out.printf("Poklad %d na: %d %d\n", i, row, col);

	   		MazeCard fieldCard = fieldList[row][col].getCard();
	   		Treasure treasure = Treasure.getTreasure(i);
	   		fieldCard.setTreasure(treasure);
		}
	}

    /**
     * Vrati poklad
     * 
     * @param code - reprezentace pokladu
     * @return	hledany poklad
     */
    public static Treasure getTreasure(int code) {
    	return treasureList.get(code);
    }
    
    /**
     * Vrati nasledujici poklad
     * 
     * @return Treasure - Vrati dalsi poklad
     */
    public static Treasure getNextTreasure() {
    	if(!treasureIterator.hasNext()) {
    		treasureIterator = treasureList.iterator();
    	}
    	
    	return treasureIterator.next();
    }
    
    /**
     * @return int - Vrati pocet pokladu
     */
    public static int getNumOfTreasure() {
    	return treasureList.size();
    }
    
    /**
     * @return int - Vrati ID pokladu
     */
    public int getCode() {
    	return code;
    }
}
