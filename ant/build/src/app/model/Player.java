package app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** 
 * Trida reprezentujici hrace
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 * 
 */
public class Player implements Serializable {
	
	private static final long serialVersionUID = -6314307597016493137L;
	
	// Seznam vsech hracu
	private static List<Player> players = new ArrayList<>();
	private static Iterator<Player> playerIterator;
	
	private int playerId;	// Jedinecne cislo hrace
    private int points;	// Pocet bodu hrace
    private boolean pushed;	// Zda bude premisten pri shiftu
    private MazeField actualField;	// Aktualni pozice hrace
    private Treasure huntedTreasure;	// Aktualni ukol hrace

    /**
     * Hrace si vytvorim podle sebe
     */
    private Player() {}
    
    /**
     * Vytvori hrace
     * 
     * @param playerId - Cislo hrace
     * @param startField - Startovni pole hrace
     */
    private Player(int id, MazeField startField) {
        playerId = id;
        points = 0;
        actualField = startField; // Aktualni pole hrace
        huntedTreasure = Treasure.getNextTreasure();
        pushed = false;	// Nebude presunut 
    }
    
    /**
     * Vytvori hrace
     * 
     * @param numOfPlayers - Kolik hracu vytvorit
     * @param mazeBoard - Hraci deska
     */
    public static void createPlayers(int numOfPlayers, MazeBoard mazeBoard) {
    	MazeField playerStartField;
    	Player newPlayer;
    	
    	for (int playerId = 0; playerId < numOfPlayers; playerId++) {
    		playerStartField = mazeBoard.getPlayerStartField(playerId);
    		newPlayer = new Player(playerId, playerStartField);
			players.add(newPlayer);
		}
    }
    
    /**
     * Posune hrace zvolenym smerem
     * 
     * @param clickedField - Plicko na ktere by se mel hrac presunout
     * @param wantedDir - LEFT, RIGHT, UP, DOWN
     * 
     */
    public void move(MazeField clickedField, MazeCard.CANGO wantedDir) {
    	MazeCard playerCard = actualField.getCard();
    	
    	if(playerCard.canGo(wantedDir)) {	// Pokud lze jit z aktualni karty
    		// Nahraji docasne promenne
    		MazeCard clickedCard = clickedField.getCard();
            switch(wantedDir) {
                case LEFT:	// Chci jit doleva
                    if (clickedCard.canGo(MazeCard.CANGO.RIGHT)) { // Lze vkrocit do vedlejsi karty zprava
                    	goNextField(clickedField);
                    }
                    break;
                case DOWN: // Chci jit dou
                    if (clickedCard.canGo(MazeCard.CANGO.UP)) {	// Lze vkrocit do vedlejsi karty zhora
                    	goNextField(clickedField);
                    }
                    break;
                case RIGHT: // Chci jit doprava
                    if (clickedCard.canGo(MazeCard.CANGO.LEFT)) {	// Lze vkrocit do vedlejsi karty zleva
                    	goNextField(clickedField);
                    }
                    break;
                case UP: // Chci jit nahoru
                    if (clickedCard.canGo(MazeCard.CANGO.DOWN)) {	// Lze vkrocit z vedlejsi karty zdolu
                    	goNextField(clickedField);
                    }
                    break;
                default:
                    throw new IllegalArgumentException();
            }
    	}      
    }
    
    /**
     * Presune hrace a da o tom vedet
     * 
     * @param newDest - Nove policko hrace
     */
    private void goNextField(MazeField newField) {
    	actualField = newField;
    	Turn.setFieldTreasure();
    	MazeBoard.showTreasureUnderPlayer();
    }
    
    /**
     * Porovna ukol hrace s pokladem na aktualnim policku
     * 
     * @param foundedTreasure - Nalezeny poklad
     * @return true = shoduji se, false = neshoduji se
     */
    public boolean compareTreasures(Treasure foundedTreasure) {
    	if(huntedTreasure.equals(foundedTreasure)) {
    		points++;
    		setHuntedTreasure();
    		return true;
		}
    	
    	return false;
    }
    
    /**
     * Vraci dalsiho hrace, po preteceni zacina odznova
     * 
     * @return Player - Dalsi hrac na rade
     */
    public static Player getNextPlayer() {
		if(playerIterator == null || !playerIterator.hasNext()) {
			playerIterator = players.iterator();
		}
		
		return (Player) playerIterator.next();
	}
	
    /**
     * @param playerId - Jedinecne cislo hrace
     * 
     * @return Player - Pozadovaneho hrace
     */
    public static Player getPlayer(int playerId) {
    	return players.get(playerId);
    }
    
    /**
     * @return List - Seznam hracu
     */
	public static List<Player> getPlayers() {
		return players;
	}
	
	/**
	 * @return int - Pocet hracu
	 */
	public static int getNumOfPlayers() {
		return players.size();
	}
    
    /**
     * @return int - Id hrace
     */
    public int getPlayerId() {
    	return playerId;
    }
    
    /**
     * @return int - Pocet bodu hrace
     */
    public int getPoints() {
        return points;
    }
    
    /**
     * @param newActualField - Nova aktualni pozice hrace
     */
    public void setActualField(MazeField newActualField) {
    	this.actualField = newActualField;
    }
    
    /**
     * @return MazeField - Aktualni policko hrace
     */
    public MazeField getActualField() {
    	return actualField;
    }
    
    /**
     * Nastavi novy hledany poklad
     */
    public void setHuntedTreasure() {
        huntedTreasure = Treasure.getNextTreasure();
    }
    
    /**
     * @return Momentalne hledany poklad
     */
    public Treasure getHuntedTreasure() {
        return huntedTreasure;
    }
    
    /**
     * @param pushed - Nastavi moznost skoku
     */
    public void setPushed(Boolean pushed) {
    	this.pushed = pushed;
    }
    
    /**
     * @param field - Policko na ktere, by mel byt hrac posunut
     * 
     */
    public void isPushed(MazeField field) {
    	if(pushed) {
    		pushed = false;
    		actualField = field;
    	}
    }
    
    /**
     * 
     * @return Radek hrace
     */
    public int getRow() {
        return actualField.getRow();
    }
    
    /**
     * 
     * @return Sloupec hrace
     */
    public int getCol() {
        return actualField.getCol();
    }
    
    @Override
	public String toString() {
		return String.valueOf(playerId);
	}
}
