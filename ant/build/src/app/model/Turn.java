package app.model;

/** 
 * Trida reprezentujici aktualni tah
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
public class Turn {
	private static Player playerOnTurn;	// Hrac na rade
	private static boolean shift; // Zda bude shift || posun
	
	// Aktualni poklad hrace + policka
	private static Treasure fieldTreasure; // Poklad na poli
	
	public Turn() {
		shift = true;
	}
	
	/**
	 * @return Player - Aktualni hrace na rade
	 */
	public static Player getPlayerOnTurn() {
		return playerOnTurn;
	}
	
	/**
	 * Nastavi hrace na radu.
	 * 
	 * @param playerOnTurn Hrac
	 */
	public static void setPlayerOnTurn(Player playerOnTurn) {
		Turn.playerOnTurn = playerOnTurn;
	}
	
	/**
	 * @return true - Pokud muze hrac shiftovat, false - pokud ne
	 */
	public static boolean isShift() {
		return shift;
	}
	
	/**
	 * @param canShift - Zda hrac smi shiftovat
	 */
	public static void setShift(boolean canShift) {
		Turn.shift = canShift;
	}
	
	/**
	 * @return Treasure - Vrati aktualni poklad na policku
	 */
	public static Treasure getFieldTreasure() {
		return fieldTreasure;
	}
	
	/**
	 * Nastavi aktualni poklad na policku
	 */
	public static void setFieldTreasure() {
		MazeField field = playerOnTurn.getActualField();
		MazeCard fieldCard = field.getCard();
		Treasure cardTreasure = fieldCard.getTreasure();

		fieldTreasure = cardTreasure;
	}
}
