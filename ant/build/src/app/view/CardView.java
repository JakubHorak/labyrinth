package app.view;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import app.lib.MyIcon;
import app.model.Player;
import app.model.Treasure;
import app.model.Turn;

/**
* Trida reprezentujici panel pro zobrazeni balicku ukolu hrace a pokladu na policku
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class CardView extends JPanel{
	
	private static CardView cardPanel;
	private static JLabel lblPlayerCard;
	private static JLabel lblFieldCard;
	
	/**
	 * Vytvori panel pro balicky karet a nastavi kontroler
	 */
	private CardView() {
		setLayout(new FlowLayout());
	}
	
	/**
	 * Vytvori panel pro balicek karet hrace a aktualni poklad na policku
	 * 
	 * @return panel s kartami a poklady
	 */
	public static CardView createCardPanel(){
		// Panel pro balicky kare
		if(cardPanel == null) {
			cardPanel = new CardView();
			cardPanel.addItems();
		}
		return cardPanel;
	}
	
	/**
	 * Nahraje polozky do panelu
	 */
	private void addItems() {
		// Ohraniceni labelu
		Border border = BorderFactory.createLineBorder(Color.BLACK, 3);
		
		// Label pro napis karta
		createCardPackLbl();
		
		// Label pro balicek pokladu hrace
		createPlayerCardLbl(border);
		
		// Label pro nadpis pokladu
		createTreasureLbl();
		
		// Label pro zobrazeni pokladu na policku
		createFieldCardLbl(border);
	}
	
	/**
	 * Label pro nazev karta
	 */
	private void createCardPackLbl() {
		// Label pro klikani na balicek
		JLabel lblCardPack = new JLabel();
		lblCardPack.setIcon(new ImageIcon(MyIcon.LIB_PATH + "karta.png"));
		add(lblCardPack);
	}
	
	/**
	 * Label pro poklady hrace
	 * 
	 * @param border - ohraniceni karty
	 */
	private void createPlayerCardLbl(Border border) {
		lblPlayerCard = new JLabel();
		lblPlayerCard.setBorder(border);
		lblPlayerCard.setIcon(MyIcon.getTreasureIcon(24));
		add(lblPlayerCard);
	}
	
	/**
	 * Label pro nazev poklad
	 */
	private void createTreasureLbl() {
		JLabel lblTreasure = new JLabel();
		lblTreasure.setIcon(new ImageIcon(MyIcon.LIB_PATH + "poklad.png"));
		add(lblTreasure);
	}
	
	/**
	 * Label pro poklad na policku
	 * 
	 * @param border - ohraniceni labelu
	 */
	private void createFieldCardLbl(Border border) {
		lblFieldCard = new JLabel();
		lblFieldCard.setBorder(border);
		lblFieldCard.setIcon(MyIcon.getTreasureIcon(24));
		add(lblFieldCard);
	}
	
	/**
	 * Prekresli aktualni kartu hrace a poklad na policku
	 */
	public static void repaintCards() {
		lblPlayerCard.setIcon(MyIcon.getTreasureIcon(24));
		lblFieldCard.setIcon(MyIcon.getTreasureIcon(24));
	}
	/**
	 * Nastavi kartu na poli
	 */
	public static void setPlayerCardIcon() {
		Player playerOnTurn = Turn.getPlayerOnTurn();
		
		Treasure playerHuntedTreasure = playerOnTurn.getHuntedTreasure();
		int treasureCode = playerHuntedTreasure.getCode();
		
		Icon treasureIcon = MyIcon.getTreasureIcon(treasureCode);
		lblPlayerCard.setIcon(treasureIcon);
	}
	
	/**
	 * Nastavi kartu na poli
	 */
	public static void setFieldCardIcon() {
		Treasure tmpTreasure = Turn.getFieldTreasure();
		Icon tmpIcon;
		// Zjistim zda je na poli poklad
		if(tmpTreasure != null) {
			tmpIcon = MyIcon.getTreasureIcon(tmpTreasure.getCode());
		}
		else {
			tmpIcon = MyIcon.getTreasureIcon(24);
		}
		lblFieldCard.setIcon(tmpIcon);
	}
}
