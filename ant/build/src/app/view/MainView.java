package app.view;

import app.controller.*;
import app.lib.MyIcon;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/** 
 * Trida reprezentujici hlavni okno hry
 * 
 * @author Jakub Horák - jakubhorak@outlook.cz
 *
 */
@SuppressWarnings("serial")
public class MainView extends JFrame {
	
	private MainViewController controller;
	private static MainView window;
	
	/**
	 * vytvori hlavni okno
	 */
	public MainView() {
		controller = new MainViewController();
		window = this;
		
		// Okno
		setSize(600,625);
		setLocationRelativeTo(null);
		setTitle("Labyrint");
		setBackground(Color.BLUE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		// Window menu
		MenuView menuPanel = MenuView.createMenu();
		getContentPane().add(menuPanel, BorderLayout.NORTH);
		setVisible(true);
	}
	
	/**
	 * Vlozi hraci desku a prislusenstvi do hlavniho okna
	 */
	public void showMainView(){
		// Status panel
		StatusView.createStatusView();
		
		// Tlacitko pro dalsi tah
		setLblNextPlayer();
		
		// Vlozim statu panel
		JPanel statusPanel = StatusView.getStatusPanel();
		add(statusPanel, BorderLayout.SOUTH);
		
		// Vlozim volnou kartu
		FreeCardView freeCardView = FreeCardView.createFreeCardView();
		getContentPane().add(freeCardView, BorderLayout.WEST);
		
		// Vytvorim box pro balicek kare a hraci desku
		Box cardBox = createBoxLayout();
		getContentPane().add(cardBox);
		
		// Prekreslim
		getContentPane().revalidate();
	}
	
	/**
	 * @return Box - S vytvorenym cardView a BoardView
	 */
	private Box createBoxLayout() {
		Box cardBox = new Box(BoxLayout.Y_AXIS); // Layout pro balicek karet a hraci desku
		Component verticalGlue = Box.createVerticalGlue();
		
		// Panel s balicky karet
		CardView cardPanel = CardView.createCardPanel();
		cardBox.add(cardPanel);
		cardBox.add(verticalGlue);
		
		// Hraci deska
		BoardView boardView = BoardView.createBoardView(this); // Hraci plocha
		cardBox.add(boardView);
		cardBox.add(verticalGlue);
		
		return cardBox;
	}
	
	/**
	 * Metoda nastavi tlacitko pro prepinani hracu
	 */
	private void setLblNextPlayer() {
		
		JLabel lblNextPlayer = new JLabel();
		lblNextPlayer.setIcon(new ImageIcon(MyIcon.LIB_PATH + "dalsi.png"));
		lblNextPlayer.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				controller.nextTurn();
			}
		});
		
		getContentPane().add(lblNextPlayer, BorderLayout.EAST);
	}
	
	/**
	 * 
	 * @return Hlavni okno aplikace
	 */
	public static MainView getInitView() {
		return window;
	}
}