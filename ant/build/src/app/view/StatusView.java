package app.view;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import app.model.Player;
import app.model.Turn;

/**
* Trida reprezentujici status bar
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class StatusView extends JPanel {
	// Status panel
	private static StatusView statusPanel;
	// Label pro vypis jmena aktualniho hrace
	private static JLabel lblPlayerName;
	// Label pro vypis bodu aktualniho hrace
	private static JLabel lblPlayerPoints;
	
	/**
	 * Konstruktor vytvori status menu
	 */
	private StatusView() {}
	
	public static StatusView createStatusView() {
		if(statusPanel == null) {
			statusPanel = new StatusView();
			statusPanel.addItems();
			refreshStatusBar();
		}
		
		return statusPanel;
	}
	
	public void addItems() {
		// Vytvorim status panel(hrac na rade, jeho body)
		setBorder(new BevelBorder(BevelBorder.LOWERED));
		setPreferredSize(new Dimension(MainView.getInitView().getWidth(), 16));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		// Polozky v panelu
		// Label s titulkem hrace
		JLabel statusLblPlayer = new JLabel("Hráč: ");
		statusLblPlayer.setHorizontalAlignment(SwingConstants.LEFT);
		
		// Label s nahranym hracem
		lblPlayerName = new JLabel("");
		lblPlayerName.setHorizontalAlignment(SwingConstants.LEFT);
		lblPlayerName.setHorizontalAlignment(SwingConstants.LEFT);
		lblPlayerName.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 50));
		
		// Label s titulkem body
		JLabel statusLblPoints = new JLabel("Body: ");
		statusLblPoints.setHorizontalAlignment(SwingConstants.LEFT);
		
		// Label s nahranymi body
		lblPlayerPoints = new JLabel("");
		lblPlayerPoints.setHorizontalAlignment(SwingConstants.LEFT);
		
		// Pridam polozky do status panelu
		add(statusLblPlayer);
		add(lblPlayerName);
		add(statusLblPoints);
		add(lblPlayerPoints);
	}
		
	/**
	 * Metoda prepise aktualniho hrace na tahu
	 */
	public static void refreshLblPlayerName() {
		Player tmpPlayer = Turn.getPlayerOnTurn(); //MazeBoard.getData().getActualPlayer();
		lblPlayerName.setText(String.valueOf(tmpPlayer.getPlayerId()));;
	}

	/**
	 * Metoda prepise aktualni body hrace
	 */
	public static void refreshLblPlayerPoints() {
		Player tmpPlayer = Turn.getPlayerOnTurn();
		lblPlayerPoints.setText(String.valueOf(tmpPlayer.getPoints()));;
	}
	
	/**
	 * Metoda prepire aktualniho hrace i jeho body
	 */
	public static void refreshStatusBar() {
		refreshLblPlayerName();
		refreshLblPlayerPoints();
	}

	/**
	 * 
	 * @return JPanel - Status panel
	 */
	public static JPanel getStatusPanel() {
		return statusPanel;
	}
	
	
}
