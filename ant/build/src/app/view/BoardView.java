package app.view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.controller.BoardViewController;
import app.lib.MyIcon;
import app.model.MazeBoard;

/**
* 
* @author Jakub Horák - jakubhorak@outlook.cz
*
*/
@SuppressWarnings("serial")
public class BoardView extends JPanel{
	
	private static BoardView gameBoard;
	private BoardViewController controller;
	private MazeBoard mazeBoard;
	private MainView mainView;
	private static JLabel[][] lblField;
	
	/**
	 * Konstruktor hraci desky
	 */
	private BoardView(MainView mainView) {
		this.mainView = mainView;
		mazeBoard = MazeBoard.getMazeBoard();
		controller = new BoardViewController(mazeBoard, this);
	}
	
	/**
	 * Vytvori novou desku, pokud jeste nebyla vytvorena
	 * 
	 * @param mainView - Hlavni okno aplikace
	 * 
	 * @return Hraci desku
	 */
	public static BoardView createBoardView(MainView mainView) {
		if(gameBoard == null) {
			gameBoard = new BoardView(mainView);
			gameBoard.addItems();
		}
		return gameBoard;
	}
	
	/**
	 * Pripravi hraci desku
	 */
	public void addItems() {
		setBoardSize();
		showBoard();
	}
	
	/**
	 * Nastavi velikost hraci plochy
	 */
	private void setBoardSize() {
		int sizeOfBoard = mazeBoard.getSizeOfBoard(); // Velikost hraci desky
		int iconSize = 50; // Velikost ikon
		
		// Nastavim hraci desku
		gameBoard.setLayout(new GridLayout(0, sizeOfBoard)); //nedefinovany pocet radku, pocet sloupcu == velikost desky
		gameBoard.setMinimumSize(new Dimension(iconSize*sizeOfBoard,iconSize*sizeOfBoard));
		gameBoard.setMaximumSize(new Dimension(iconSize*sizeOfBoard,iconSize*sizeOfBoard));
		
		if(sizeOfBoard > 7)
		{
			mainView.setSize(650 + 50*((sizeOfBoard-7)), 600 + 50*(sizeOfBoard-7));
		}
	}
	
	/**
	 * Nahraje polozky na hraci plochu
	 */
	public void showBoard() {
		int sizeOfBoard = mazeBoard.getSizeOfBoard(); // Velikost desky
	
		gameBoard.removeAll();
		// Pole label na desce, predstavujici jedtnotliva policka
        lblField = new JLabel[sizeOfBoard+1][sizeOfBoard+1];
        
        Icon lblFieldIcon;
        // Nastavim policka na desce
		for(int row = 1; row <= sizeOfBoard; row ++) { // Sloupce
			for(int col = 1; col <= sizeOfBoard; col++) { //Radky
				lblField[row][col] = new JLabel(); // Vytvorim label(policko)
				lblField[row][col].addMouseListener(controller); // Pridam Listener
				
				// Nastavim ikonu
				lblFieldIcon = MyIcon.setFieldIcon(row, col);
				lblField[row][col].setIcon(lblFieldIcon);
				// Pridam do panelu
				gameBoard.add(lblField[row][col]);
			}
		}
		MyIcon.setPlayerIcon();
        gameBoard.revalidate();  
	}
	
	public static JLabel getLblField(int row, int col) {
		return lblField[row][col];
	}
	
	/**
	 * Vycisti hraci desku
	 */
	public void clearGameBoard() {
		gameBoard.removeAll();
	}
}
