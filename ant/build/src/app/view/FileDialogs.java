package app.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JTextField;


/** Třída pro dialogy(model)
 * 
 * @author xhorak61
 *
 */
public class FileDialogs extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private transient JTextField filename = new JTextField();
	private transient JTextField dir = new JTextField();

	public FileDialogs(JMenuItem save, JMenuItem open) {
		open.addActionListener(new OpenL());
	    save.addActionListener(new SaveL());
	}

	class OpenL implements ActionListener {
		 
		public void actionPerformed(ActionEvent e) {
			JFileChooser c = new JFileChooser();
			// Demonstrate "Open" dialog:
			int rVal = c.showOpenDialog(FileDialogs.this);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				filename.setText(c.getSelectedFile().getName());
				dir.setText(c.getCurrentDirectory().toString());
				//MazeBoard.loadGame("examples/"+c.getSelectedFile().getName());
				//IniView.getInitView().reload();
			}
	    }
	}

	  class SaveL implements ActionListener {
		public void actionPerformed(ActionEvent e) {
	      JFileChooser c = new JFileChooser();
	      // Demonstrate "Save" dialog:
	      int rVal = c.showSaveDialog(FileDialogs.this);
	      if (rVal == JFileChooser.APPROVE_OPTION) {
	        filename.setText(c.getSelectedFile().getName());
	        dir.setText(c.getCurrentDirectory().toString());
	        //MazeBoard.saveGame("examples/"+c.getSelectedFile().getName());
	      }
	    }
	  }
}